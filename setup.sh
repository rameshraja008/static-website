#!/bin/bash
# CodeCommit Pipeline setup file for new EC2 instance
# Version 0.8 - 20th April 2020.

# Start by removing all older web content files
cd
sudo service httpd stop
sudo rm -rf /var/www/html/*

# Now change to the web content folder
cd /var/www/html

